const express =  require('express');

const test = require('../v1/test');

const router = express.Router();

router.post('/postrequest',   test.postRequest);
router.put('/putrequest', test.putRequest);
router.delete('/deleterequest/:deleteId',  test.deleteRequest);
router.get('/getrequest' , test.getRequest);

module.exports = router;
