// Include Sequelize module. 
const Sequelize = require('sequelize') 
const sequelize = require('../utils/database') 

// Define method takes two arrguments 
// 1st - name of table 
// 2nd - columns inside the table 
const Test = sequelize.define('test', { 

	
	testId:{ 

		// Sequelize module has INTEGER Data_Type. 
		type:Sequelize.INTEGER, 

		// To increment user_id automatically. 
		autoIncrement:true, 

		// user_id can not be null. 
		allowNull:false, 

		// For uniquely identify user. 
		primaryKey:true
	}, 
    name: { type: Sequelize.STRING, allowNull:true  },
    emailAddress: { type: Sequelize.STRING, allowNull:true },
	// Timestamps 
	createdAt: { type: Sequelize.DATE, defaultValue: Sequelize.NOW},
	updatedAt: Sequelize.DATE, 
	
}
,
{
	freezeTableName: true
}) 

// Exporting Test, using this constant 
// we can perform CRUD operations on 
// 'test' table. 
module.exports = Test
