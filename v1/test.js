var express = require('express');
var bodyParser = require('body-parser');
var conn = require('../utils/connection');

var app = express();

module.exports = {
    postRequest: (req, res) => {
        console.log("in post request method");
        // var jsondata = JSON.parse(req.body.json);

        var name = req.body.name;
        var emailAddress = req.body.emailAddress;

        if (name == null || name == "" || name == undefined) {
            res.json({ 'status': 'false', 'number': '101', 'message': 'Name cannot be empty' });
        }
        else if(emailAddress == null || emailAddress == "" || emailAddress == undefined) {
            res.json({ 'status': 'false', 'number': '102', 'message': 'Email address cannot be empty' });
        }
        else {
            var sqlquery1 = "INSERT INTO `test` ( `name`, `emailAddress`) VALUES ('" + name + "','" + emailAddress + "')";
            conn.query(sqlquery1 , (err , result2) => {

                if(err){
                    res.json({ 'status': 'False', 'number': '18', 'message': 'Error in insertion' });
                }
                else {
                    res.json({ 'status': 'True', 'msg': 'Successfully added', data: result2 });
                }
            })

        }    
    },
    putRequest: (req, res) => {
        console.log("in put request method");   
    },
    deleteRequest: (req, res) => {
        console.log("in delete request method");  
        var id = req.params.deleteId;
        console.log(" id"+id);
        var sql1 = " SELECT * FROM `test` WHERE `testId` = '" + id  + "'";
        conn.query(sql1, (err, result2) => {
            if (err) {

                res.json({ 'status': 'False', 'number': '21', 'message': 'Error in delete' });


            } else if (result2.length == 0) {

                res.json({ 'status': 'False', 'number': '22', 'message': 'Id is not present' });


            }
            else {


                var sql = " DELETE FROM `test` WHERE `testId` = '" + id + "'";

                    conn.query(sql, (err, result) => {

                        if (err) {

                            res.json({ 'status': 'False', 'number': '20', 'message': 'Error in delete' });

                        }
                        else {

                            res.json({ 'status': 'True', 'msg': 'Successfully Deleted' });

                        }

                    })

            }
        })
    },
    getRequest: (req, res) => {
        console.log("in get request method");
        
    }

}







